# easyDOS
A witch holds the key, but they know some old school tricks.
## Requirements
```pip install pygame```<br/><br/>
```sudo apt install python-gi```
### Walkthrough
Continue through the menus as the print onscreen. Each option leads to the same ending.<br/><br/>
The witch is looking for CTF-typical linux command injection lines. A one minute timer starts when they ask you a question.<br/><br/>
Type ```cat flag.txt``` to print the flag onscreen before the minute timer is up.<br/><br/>
If the `TECHNO` timeout is activated, you must restart the script to try again.